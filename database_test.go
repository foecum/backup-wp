package main

import "testing"

func TestNewDBStore(t *testing.T){
	username := "tester"
	password:= "test"
	host := "localhost"
	port := "5432"
	dbName := "test-db"
	store := NewDBStore(username, password, host, port, dbName)

	if store.dbName != dbName{
		t.Errorf("expected %s but got %s", dbName, store.dbName)
	}

	if store.port != port{
		t.Errorf("expected %s but got %s", port, store.port)
	}

	if store.host != host{
		t.Errorf("expected %s but got %s", host, store.host)
	}

	if store.password != password{
		t.Errorf("expected %s but got %s", password, store.password)
	}

	if store.username != username{
		t.Errorf("expected %s but got %s", username, store.username)
	}
}