package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/JamesStewy/go-mysqldump"
	_ "github.com/go-sql-driver/mysql"
)

type DBStore struct {
	username string
	password string
	host     string
	port     string
	dbName   string
	db       *sql.DB
	dumpFilenameFormat string
}

func NewDBStore(username, password, host, port, dbName string) *DBStore{
	log.SetOutput(os.Stdout)
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	return &DBStore{
		username:           username,
		password:           password,
		host:               host,
		port:               port,
		dbName:             dbName,
	}
}

func (d *DBStore) openConnection() error{
	var err error
	t := time.Now()
	d.dumpFilenameFormat = fmt.Sprintf("%s-%s.sql", d.dbName, t.Format(time.RFC3339))

	d.db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", d.username, d.password, d.host, d.port, d.dbName))
	if err != nil {
		log.Printf("error opening database: %v\n", err)
		return err
	}
	return nil
}

func (d *DBStore) DumpDatabase() error{
	d.openConnection()

	dir, err := os.Getwd()
	if err != nil {
		log.Printf("could not get current working directory: %v\n", err)
		return err
	}

	dumpDir := fmt.Sprintf("%s/dumps", dir)
	dumper, err := mysqldump.Register(d.db, dumpDir, d.dumpFilenameFormat)
	if err != nil {
		log.Printf("error registering database: %v\n", err)
		return err
	}

	// Dump database to file
	resultFilename, err := dumper.Dump()
	if err != nil {
		log.Printf("error creating database: %v\n", err)
		return err
	}
	log.Printf("file is saved to %s\n", resultFilename)
	dumper.Close()

	return nil
}